# Passive Income Calculator

# Getting Started
```
1. git clone git@gitlab.com:louiswilbrink/how-much-rent-io.git 
2. cd passive-income-calculator
3. nvm use
4. npm install
5. npm start
6. Navigate to `http://localhost:4200/`
```

# Tech Stack

1. Backend: Node/Express
2. Frontend: Angular
3. Database: PostgreSQL (coming soon)
4. ORM: Sequelize (coming soon)
5. Module Loader: Webpack (coming soon)
6. Taskrunner: Gulp (coming soon)
7. Test Framework: Jasmine/[Cyress](https://www.cypress.io/) (coming soon)
8. Analytics: Google Analytics (coming soon)

# Workflow

Each feature branch name should use the following naming convention: `{tracking-id}/{feature/bug/chore}-{name}/{description}`.  For example: `PIC-6/feature-property/add-mortage-pane`.

Delete branches after merging to `develop`.  Other than `master` and `develop`, the only branches that should be on Gitlab are merge requests and in-progress branches.

Before initiating a Merge Request, make sure to squash all of your commits.  The commit message should be titled to match your branch name.  For example: `PIC-6/feature-property: add mortgage pane`.  After this title line, include other notes in a bulleted list.

Target `develop` for all merge requests.  Product owner is responsible for merging `develop` into `master` and pushing to GitLab.  Production pushes are made from `master`.

## Feature Categories

All feature branches should denote a single feature category that the work pertains to.  Use the feature category that is most specific.  If your work touches multiple features, consider breaking up the commit into multiple commits.

1. App
  1. Property
2. Backend
3. Infrastructure

## APIs

## Directory Structure

## Database

## Manually access to the database

## Deployment

The app is run out of an S3 bucket configured for static hosting.  Cloudfront (CDN) is used to deliver the assets quickly.  To deploy:

1. `ng build --prod`.
2. `cd dist/`.
3. `aws s3 sync . s3://passive-income-calculator --delete`

## Analytics

`gocashflowapp.com` uses Google Analytics and [angulartics2](https://github.com/angulartics/angulartics2).

Assign the feature category to the `category` parameter.  For examples, when tracking how many times the Share button is clicked, the category would be `property`.

Each `action` should denote a verb that describes how the user acted on a noun in the imperative.

## Security

## Testing

Currently, `gocashflowapp.com` does not write tests for the codebase.  This is due to development bandwidth and the high probability of heavy refactoring milestones ahead.

Before launch, the most crucial workflows will have end-to-end tests written.  Should any revenue-generating workflows be incorporated into the app, integration and end-to-end tests using [Cypress](https://www.cypress.io/) will be written in order to ensure earnest implementation of contractual obligations.

# Angular CLI `README.md`

The remaining sections are carry-overs from the Angular CLI README.  They are not specific to `gocashflowapp.com`'s codebase, conventions, or architecture.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
