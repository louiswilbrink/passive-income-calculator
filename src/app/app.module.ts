import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { LocalStorageModule } from 'angular-2-local-storage';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';

import { ClipboardModule } from 'ngx-clipboard';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { CalculatorComponent } from './calculator/calculator.component';
import { CalcHeaderComponent } from './calculator/calc-header/calc-header.component';
import { IncomeComponent } from './calculator/income/income.component';
import { BreakdownComponent } from './calculator/breakdown/breakdown.component';

import { CalculatorService } from './calculator/calculator.service';
import { MortgagePaneComponent } from './calculator/mortgage-pane/mortgage-pane.component';
import { ExpensesPaneComponent } from './calculator/expenses-pane/expenses-pane.component';
import { ShareComponent } from './calculator/share/share.component';
import { UniversalInputComponent } from './calculator/universal-input/universal-input.component';
import { ActiveFieldDirective } from './calculator/universal-input/active-field.directive';
import { CamelToHumanPipe } from './camel-to-human.pipe';
import { SharePaneComponent } from './calculator/share-pane/share-pane.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    Ng2DeviceDetectorModule.forRoot(),
    LocalStorageModule.withConfig({
      prefix: 'gca',
      storageType: 'localStorage'
    }),
    ClipboardModule
  ],
  declarations: [
    AppComponent,
    CalculatorComponent,
    CalcHeaderComponent,
    IncomeComponent,
    BreakdownComponent,
    MortgagePaneComponent,
    ExpensesPaneComponent,
    ShareComponent,
    UniversalInputComponent,
    ActiveFieldDirective,
    CamelToHumanPipe,
    SharePaneComponent
  ],
  providers: [ CalculatorService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
