import { Angulartics2GoogleAnalytics } from 'angulartics2';
import { Component } from '@angular/core';
import { Ng2DeviceService } from 'ng2-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  height = '100vh';

  constructor(private deviceService: Ng2DeviceService, angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics) {
    // Injecting Angulartics2 required in top-level component.
    
    // Safari overlays a chrome banner on the bottom of the viewport.
    // This chrome banner is not included in the vh calculations, therefore
    // the app needs to shorten the view dynamically.
    //
    // Note: The userAgent in Firefox on iOS reports the browser as
    // 'Safari'.  An extra check for the string 'FxiOS' is needed to
    // ascertain if the browser is Firefox.
    if (deviceService.browser === 'safari' &&
      !deviceService.userAgent.includes('FxiOS')) {
      this.height = 'calc(100vh - 70px)';
    } else {
      this.height = '100vh'; // works as expected on Chrome/FF.
    }
  }
}
