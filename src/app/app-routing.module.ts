import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Angulartics2Module, Angulartics2GoogleAnalytics } from 'angulartics2';

import { CalculatorComponent } from './calculator/calculator.component';

const routes: Routes = [
  { path: '', redirectTo: '/property', pathMatch: 'full' },
  { path: 'property',   component: CalculatorComponent }
];

@NgModule({
  imports: [ 
    RouterModule.forRoot(routes),
    Angulartics2Module.forRoot([ Angulartics2GoogleAnalytics ])
  ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
