import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Angulartics2 } from 'angulartics2';
import { LocalStorageService } from 'angular-2-local-storage';

import { ActivatedRoute, ParamMap } from '@angular/router';

import { MortgageFactors } from './mortgage-factors';
import { Expenses } from './expenses';
import { Factor } from './factor';

@Component({
  selector: 'pic-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})

export class CalculatorComponent implements OnInit {
  router;
  route;
  isMortgagePaneOpen: string;
  isExpensesPaneOpen: string;
  isSharePaneOpen: string;
  isInputOpen: string;
  urlParams: string;
  mortgage: number;
  mortgageOnly: number;
  totalExpenses: number;
  income: number;
  rent: number;
  cashOnCash: number;
  principal: number;
  instructionsShown: boolean;
  isBreakdownDimmed: boolean;
  isShareDimmed: boolean;
  mortgageFactors;
  expenses;
  factor;
  angulartics;
  localStorage;
  shareLink;

  constructor(private localStorageService: LocalStorageService, 
    angulartics2: Angulartics2,
    route: ActivatedRoute,
    router: Router) {

    this.angulartics = angulartics2;
    this.localStorage = localStorageService;

    this.router = router;
    this.route = route;

    this.isMortgagePaneOpen = 'void';
    this.isExpensesPaneOpen = 'void';
    this.isSharePaneOpen = 'void';
    this.isInputOpen = 'void';
    this.mortgage = 0;
    this.mortgageOnly = 0;
    this.cashOnCash = 0;
    this.principal = 0;

    this.instructionsShown = this.localStorage.get('instructions-shown') || false;
    this.isBreakdownDimmed = !this.instructionsShown;
    this.isShareDimmed = !this.instructionsShown;

    let params;

    if (route.snapshot.params.gca) {
      // Read URL params to set cashflow factors.
      // Convert each parameter to number, exclude if any convert to NaN.
      params = route.snapshot.params.gca.split('-').map(p => +p).filter(p => !isNaN(p));

      // One of the parameters are invalid.
      if (params.length !== 14) { 
        params = new Array(14).fill(null); // throw away all params.
        this.router.navigateByUrl('/property'); // Clear the url.
      }
    } else {
      params = new Array(14).fill(null); // fill with nulls
    }

    let purchasePrice  = this.localStorage.get('purchasePrice');
    let downPayment    = this.localStorage.get('downPayment');
    let rate           = this.localStorage.get('rate');
    let loanTerm       = this.localStorage.get('loanTerm');
    let propertyTax    = this.localStorage.get('propertyTax');
    let hoi            = this.localStorage.get('hoi');

    let associationFee = this.localStorage.get('associationFee');
    let vacancyRate    = this.localStorage.get('vacancyRate');
    let vacancy        = this.localStorage.get('vacancy');
    let utilities      = this.localStorage.get('utilities');
    let maintenance    = this.localStorage.get('maintenance');
    let other          = this.localStorage.get('other');
    let management     = this.localStorage.get('management');

    this.rent          = this.localStorage.get('rent');

    purchasePrice      = +params[0]  ? +params[0]  : purchasePrice !== null  ? purchasePrice  : 180000;
    downPayment        = +params[1]  ? +params[1]  : downPayment !== null    ? downPayment    : 36000;
    rate               = +params[2]  ? +params[2]  : rate !== null           ? rate           : 0.041;
    loanTerm           = +params[3]  ? +params[3]  : loanTerm !== null       ? loanTerm       : 30;
    propertyTax        = +params[4]  ? +params[4]  : propertyTax !== null    ? propertyTax    : 545;
    hoi                = +params[5]  ? +params[5]  : hoi !== null            ? hoi            : 500;

    associationFee     = +params[6]  ? +params[6]  : associationFee !== null ? associationFee : 90;
    vacancyRate        = +params[7]  ? +params[7]  : vacancyRate !== null    ? vacancyRate    : 0.05;
    vacancy            = +params[8]  ? +params[8]  : vacancy !== null        ? vacancy        : 84.5;
    utilities          = +params[9]  ? +params[9]  : utilities !== null      ? utilities      : 45;
    maintenance        = +params[10] ? +params[10] : maintenance !== null    ? maintenance    : 110;
    other              = +params[11] ? +params[11] : other !== null          ? other          : 0;
    management         = +params[12] ? +params[12] : management !== null     ? management     : 60;

    this.rent          = +params[13] ? +params[13] : this.rent !== null      ? this.rent      : 1672;

    this.mortgageFactors = new MortgageFactors(purchasePrice, downPayment, rate, loanTerm, propertyTax, hoi);
    this.expenses = new Expenses(associationFee, vacancyRate, vacancy, utilities, maintenance, other, management);
    this.factor = new Factor('factor', '0', false);

    this.shareLink = 'https://gocashflowapp.com';
    this.urlParams = '';
  }

  ngOnInit() {
    this.calcMortgage();
    this.calcTotalExpenses();
    this.calcIncome();
    this.calcCashOnCash();
    this.calcPrincipal();

    if (this.route.snapshot.params.gca) {
      this.buildShareLink();
    }
  }

  buildShareLink() {
    this.urlParams = ['/property;gca=' + this.mortgageFactors.purchasePrice,
      this.mortgageFactors.downPayment,
      this.mortgageFactors.rate,
      this.mortgageFactors.loanTerm,
      this.mortgageFactors.propertyTax,
      this.mortgageFactors.hoi,
      this.expenses.associationFee,
      this.expenses.vacancyRate,
      this.expenses.vacancy,
      this.expenses.utilities,
      this.expenses.maintenance,
      this.expenses.other,
      this.expenses.management,
      this.rent].join('-');

    this.shareLink = ['https://gocashflowapp.com', this.urlParams].join('');
  }

  calcIncome() {
    this.income = this.rent - this.mortgage - this.totalExpenses;
    
    this.calcCashOnCash();
  }

  calcMortgage() {
    // Assign to local variables to make calculations easier to read.
    var loan = this.mortgageFactors.purchasePrice - this.mortgageFactors.downPayment;
    var monthlyRate = this.mortgageFactors.rate/12;
    var months = this.mortgageFactors.loanTerm * 12;
    var propertyTax = this.mortgageFactors.propertyTax/12;
    var hoi = this.mortgageFactors.hoi/12;

    this.mortgageOnly = (loan * (monthlyRate * Math.pow((1 + monthlyRate), months)))/(Math.pow((1 + monthlyRate), months) - 1);

    this.mortgage = this.mortgageOnly + (propertyTax + hoi);

    this.calcIncome();
    this.calcPrincipal();
  }
  
  calcTotalExpenses() {
    this.totalExpenses = this.expenses.associationFee + this.expenses.vacancy + this.expenses.utilities + this.expenses.maintenance + this.expenses.other + this.expenses.management;
    
    this.calcIncome();
  }

  calcCashOnCash() {
    this.cashOnCash = (this.income * 12) / this.mortgageFactors.downPayment;
  }

  calcPrincipal() {
    var monthlyRate = this.mortgageFactors.rate/12;
    var loan = this.mortgageFactors.purchasePrice - this.mortgageFactors.downPayment;

    this.principal = this.mortgageOnly - (loan * monthlyRate);
  }

  updateFactor(factor) {
    // This tracking must be upstream to prevent multiple event triggers.
    // Assumption is that every factor update will trigger an income recalculation.
    this.angulartics.eventTrack.next({ action: 'income-calculated', properties: { category: 'property' }});

    switch (factor.name) {
      case 'rent':
        this.rent = +factor.value;
        this.expenses.setVacancy(this.rent);;
        this.calcIncome();
        break;
      case 'purchasePrice':
        this.mortgageFactors.setPurchasePrice(+factor.value);
        this.calcMortgage();
        break;
      case 'downPayment':
        this.mortgageFactors.setDownPayment(+factor.value);
        this.calcMortgage();
        break;
      case 'rate':
        this.mortgageFactors.setRate(+((+factor.value).toFixed(5))); 
        this.calcMortgage();
        break;
      case 'loanTerm':
        this.mortgageFactors.setLoanTerm(+factor.value);
        this.calcMortgage();
        break;
      case 'propertyTax':
        this.mortgageFactors.setPropertyTax(+factor.value);
        this.calcMortgage();
        break;
      case 'hoi':
        this.mortgageFactors.setHoi(+factor.value);
        this.calcMortgage();
        break;
      case 'associationFee':
        this.expenses.setAssociationFee(+factor.value);
        this.calcTotalExpenses();
        break;
      case 'vacancyRate':
        this.expenses.setVacancyRate(+factor.value);
        this.expenses.setVacancy(this.rent);;
        this.calcTotalExpenses();
        break;
      case 'utilities':
        this.expenses.setUtilities(+factor.value);
        this.calcTotalExpenses();
        break;
      case 'maintenance':
        this.expenses.setMaintenance(+factor.value);
        this.calcTotalExpenses();
        break;
      case 'other':
        this.expenses.setOther(+factor.value);
        this.calcTotalExpenses();
        break;
      case 'management':
        this.expenses.setManagement(+factor.value);
        this.calcTotalExpenses();
        break;
      default:
        console.log('calculator.updateFactor(): no factors updated');
    }
    
    this.angulartics.eventTrack.next({ 
      action: factor.name.replace(/([A-Z])/g, function($1){return "-"+$1.toLowerCase();}) + '-updated', 
      properties: { category: 'property' }
    });

    if (this.route.snapshot.params.gca) {
      this.router.navigateByUrl('/property'); // Clear the url parameters.
    }

    this.syncLocalStorage();
    this.buildShareLink();
    this.closeInput();
  }

  syncLocalStorage() {
    this.localStorage.set('rent', this.rent);
    this.localStorage.set('purchasePrice', this.mortgageFactors.purchasePrice);
    this.localStorage.set('downPayment', this.mortgageFactors.downPayment);
    this.localStorage.set('rate', this.mortgageFactors.rate);
    this.localStorage.set('loanTerm', this.mortgageFactors.loanTerm);
    this.localStorage.set('propertyTax', this.mortgageFactors.propertyTax);
    this.localStorage.set('hoi', this.mortgageFactors.hoi);
    this.localStorage.set('associationFee', this.expenses.associationFee);
    this.localStorage.set('vacancyRate', this.expenses.vacancyRate);
    this.localStorage.set('vacancy', this.expenses.vacancy);
    this.localStorage.set('utilities', this.expenses.utilities);
    this.localStorage.set('maintenance', this.expenses.maintenance);
    this.localStorage.set('other', this.expenses.other);
    this.localStorage.set('management', this.expenses.management);
  }

  updateMortgageFactors(factors: MortgageFactors) {
    this.mortgageFactors.setPurchasePrice(factors.purchasePrice);
    this.mortgageFactors.setDownPayment(factors.downPayment);
    this.mortgageFactors.setRate(factors.rate);
    this.mortgageFactors.setPropertyTax(factors.propertyTax);
    this.mortgageFactors.setHoi(factors.hoi);

    this.calcMortgage();
  }

  openInput(factor) {
    this.factor = factor;
    this.isInputOpen = '*';
  }

  closeInput() {
    this.isInputOpen = 'void'
  }

  closePane(paneName) {
    switch (paneName) {
      case 'expenses':
        this.isExpensesPaneOpen = 'void';
        break;
      case 'mortgage':
        this.isMortgagePaneOpen = 'void';
        break;
      case 'share':
        this.isSharePaneOpen = 'void';
        break;
      default:
        console.log('no pane closed');
    }
  }

  openPane(paneName) {
    switch (paneName) {
      case 'expenses':
        this.isExpensesPaneOpen = '*';
        this.angulartics.eventTrack.next({ action: 'expenses-pane-opened', properties: { category: 'property' }});
        break;
      case 'mortgage':
        this.isMortgagePaneOpen = '*';
        this.angulartics.eventTrack.next({ action: 'mortgage-pane-opened', properties: { category: 'property' }});
        break;
      case 'share':
        this.isSharePaneOpen = '*';
        this.angulartics.eventTrack.next({ action: 'share-pane-opened', properties: { category: 'property' }});
        break;
      default:
        console.log('no pane closed');
    }
  }

  shareCashflow() {
    console.log('shareCashflow()');
  }

  clear() {
    // reset all factors.
    this.rent = 1672;

    this.mortgageFactors = new MortgageFactors(180000, 36000, 0.041, 30, 545, 500);
    this.expenses = new Expenses(90, 0.05, 84.5, 45, 110, 0, 60);
    this.factor = new Factor('factor', '0', false);

    this.shareLink = 'https://gocashflowapp.com';
    this.urlParams = '';

    this.calcMortgage();
    this.calcTotalExpenses();
    this.calcIncome();
    this.calcCashOnCash();
    this.calcPrincipal();

    // Clear values persisted in localStorage.
    this.localStorage.clearAll();

    // Restore this counter 
    // (it's faster to delete everything and restore one value)
    this.localStorage.set('instructions-shown', this.instructionsShown);

    this.router.navigateByUrl('/property'); // Clear the url parameters.
  }

  disableInstructions() {
    this.instructionsShown = true;

    this.localStorage.set('instructions-shown', this.instructionsShown);

    this.isBreakdownDimmed = false;
    this.isShareDimmed = false;
  }
}
