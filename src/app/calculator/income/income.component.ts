import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'pic-income',
  templateUrl: './income.component.html',
  styleUrls: ['./income.component.scss'],
	animations: [
	trigger('showHide', [
		state('void', style({
			top: '20px',
			opacity: '0'
		})),
		state('*', style({
			top: '0px',
			opacity: '1'
		})),
		transition('void => *', animate('200ms ease-out')),
		transition('* => void', animate('200ms ease-in')),
	])
]
})

export class IncomeComponent implements OnInit {
  _showInstructions: boolean;
  _showIncome: boolean;
  _showLabel: boolean;
  localStorage;

  @Input() income: number;
  @Input() instructionsShown: boolean;
  @Output() onShowIncome = new EventEmitter<string>();

  constructor(private localStorageService: LocalStorageService) {
    this.localStorage = localStorageService;

    this._showInstructions = true;
    this._showIncome = false;

    this._showLabel = this.localStorage.get('income._showLabel') !== undefined ? 
      this.localStorage.get('income._showLabel') : true;
  }

  ngOnInit() {
    if (this.instructionsShown) {
      this._showInstructions = false;
      this._showIncome = true;
    }
  }

  onIncomeClicked() {
    if (this.instructionsShown || this._showIncome) {
      this.toggleLabel();
    } else {
      this.hideInstructions();
    }
  }

  toggleLabel() {
    this._showLabel = !this._showLabel;
    this.localStorage.set('income._showLabel', this._showLabel);
  }

  hideInstructions() {
    this._showInstructions = false;
  }

  showIncome() {
    if (!this._showInstructions) {
      this._showIncome = true;
      this.onShowIncome.emit();
    }
  }
}
