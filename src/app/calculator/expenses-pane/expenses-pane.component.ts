import { Component, EventEmitter, Input, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { Expenses } from '../expenses';
import { Factor } from '../factor';

@Component({
  selector: 'pic-expenses-pane',
  templateUrl: './expenses-pane.component.html',
  styleUrls: ['./expenses-pane.component.scss'],
  animations: [
    trigger('openPane', [
      state('void', style({
        position: 'absolute',
        left: '-414px',
      })),
      state('*', style({
        position: 'absolute',
        left: '0px',
      })),
      transition('void => *', animate('300ms ease-out')),
      transition('* => void', animate('300ms ease-in')),
    ])
  ]
})
export class ExpensesPaneComponent {
  @Input() isOpen: boolean = false;
  @Input() expenses: Expenses;
  @Output() onClosed = new EventEmitter<string>();
  @Output() onFactorClicked = new EventEmitter<Factor>();
  
  closePane() {
    this.onClosed.emit('expenses');
  }

  updateExpense(name, value, isRate: boolean = false, decimalPoint: number = -1) {
    let factor = new Factor(name, value.toString(), isRate, decimalPoint);
    this.onFactorClicked.emit(factor);
  }

  constructor() { }
}
