import { Component, EventEmitter, Input, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { Factor } from '../factor';
import { MortgageFactors } from '../mortgage-factors';

@Component({
  selector: 'pic-mortgage-pane',
  templateUrl: './mortgage-pane.component.html',
  styleUrls: ['./mortgage-pane.component.scss'],
  animations: [
    trigger('openPane', [
      state('void', style({
        position: 'absolute',
        left: '-414px'
      })),
      state('*', style({
        position: 'absolute',
        left: '0px'
      })),
      transition('void => *', animate('300ms ease-out')),
      transition('* => void', animate('300ms ease-in')),
    ])
  ]
})
export class MortgagePaneComponent {
  _factors: Factor[];

  @Input() isOpen: string;
  @Input() mortgageFactors: MortgageFactors;

  @Output() onClosed = new EventEmitter<string>();
  @Output() onFactorClicked: EventEmitter<object> = new EventEmitter();

  constructor() {
    this._factors = [];
  }

  closePane() {
    this.onClosed.emit('mortgage');
  }

  updateMortgageFactor(name, value, isRate: boolean = false, decimalPoint: number = -1) {
    let factor = new Factor(name, value.toString(), isRate, decimalPoint);
    this.onFactorClicked.emit(factor);
  }
}
