import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MortgagePaneComponent } from './mortgage-pane.component';

describe('MortgagePaneComponent', () => {
  let component: MortgagePaneComponent;
  let fixture: ComponentFixture<MortgagePaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MortgagePaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MortgagePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
