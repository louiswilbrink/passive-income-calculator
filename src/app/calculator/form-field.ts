export class FormField {
  constructor(public name: string = '',
    public value: string = '',
    public isValid: boolean = true) {
  }

  setName(name: string) {
    this.name = name;
  }

  setValue(value: string) {
    this.value = value;
  }
  
  setIsValid(isValid: boolean) {
    this.isValid = isValid;
  }
}
