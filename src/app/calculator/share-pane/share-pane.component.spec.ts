import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharePaneComponent } from './share-pane.component';

describe('SharePaneComponent', () => {
  let component: SharePaneComponent;
  let fixture: ComponentFixture<SharePaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharePaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
