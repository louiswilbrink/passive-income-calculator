import { Component, EventEmitter, Input, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { Angulartics2 } from 'angulartics2';

import { MortgageFactors } from '../mortgage-factors';
import { Expenses } from '../expenses';

@Component({
  selector: 'gca-share-pane',
  templateUrl: './share-pane.component.html',
  styleUrls: ['./share-pane.component.scss'],
  animations: [
    trigger('openPane', [
      state('void', style({
        position: 'absolute',
        bottom: '-100%',
      })),
      state('*', style({
        position: 'absolute',
        bottom: '0px',
      })),
      transition('void => *', animate('300ms ease-out')),
      transition('* => void', animate('300ms ease-in')),
    ]),
    trigger('hideLink', [
      state('void', style({
        position: 'absolute',
        opacity: '0',
        top: '95px',
      })),
      state('*', style({
        position: 'absolute',
        opacity: '1',
        top: '45px',
      })),
      transition('void => *', animate('150ms ease-out')),
      transition('* => void', animate('150ms ease-in')),
    ]),
    trigger('showConfirmation', [
      state('void', style({
        position: 'absolute',
        opacity: '0',
        top: '95px',
      })),
      state('*', style({
        position: 'absolute',
        opacity: '1',
        top: '0px',
      })),
      transition('void => *', animate('150ms ease-out')),
      transition('* => void', animate('150ms ease-in')),
    ]),
  ]
})
export class SharePaneComponent {
  angulartics;

  _hideLink: boolean;
  _showConfirmation: boolean;

  @Input() isOpen: string;
  @Input() urlParams: string;
  @Input() shareLink: string;

  @Output() onClose = new EventEmitter<string>();

  constructor(angulartics2: Angulartics2) {
    this.angulartics = angulartics2;

    this._hideLink = false;
    this._showConfirmation = false;
  }

  closePane() {
    this.onClose.emit('share');
  }

  showConfirmation() {
    if (this._hideLink) {
      this._showConfirmation = true;
    }
  }

  resetAnimations() {
    this._hideLink = false;
    this._showConfirmation = false;
  }

  onCopyLink() {
    this.angulartics.eventTrack.next({ action: 'copy-to-clipboard-clicked', properties: { category: 'property' }});

    this._hideLink = true;
  }
}
