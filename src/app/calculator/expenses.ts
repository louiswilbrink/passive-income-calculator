export class Expenses {
  constructor(public associationFee: number,
    public vacancyRate: number,
    public vacancy: number, 
    public utilities: number,
    public maintenance: number,
    public other: number,
    public management: number) {
  }

  setAssociationFee(associationFee: number) {
    this.associationFee = associationFee;
  }

  setVacancyRate(vacancyRate: number) {
    this.vacancyRate = vacancyRate;
  }

  setVacancy(rent) {
    this.vacancy = +((((rent * 12) * this.vacancyRate) / 12).toFixed(2));
  }

  setUtilities(utilities: number) {
    this.utilities = utilities;
  }

  setMaintenance(maintenance: number) {
    this.maintenance = maintenance;
  }

  setManagement(management: number) {
    this.management = management;
  }

  setOther(other: number) {
    this.other = other;
  }
}
