import { Component, OnInit, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { Factor } from '../factor';

@Component({
  selector: 'pic-breakdown',
  templateUrl: './breakdown.component.html',
  styleUrls: ['./breakdown.component.scss'],
  animations: [
    trigger('showHide', [
      state('void', style({
        opacity: '0',
        transform: 'translateX(100%)'
      })),
      state('*', style({
        opacity: '1'
      })),
      transition('void => *', animate('300ms ease-out')),
    ])
  ]
})
export class BreakdownComponent {
  _rent: Factor;
  _metric: string;
  _showMortgageBorder: boolean;
  _showRentBorder: boolean;
  _showExpensesBorder: boolean;
  _showCashOnCashBorder: boolean;

  constructor() {
    this._rent = new Factor('name', '0', false);
    this._metric = 'default';

    this._showMortgageBorder = false;
    this._showRentBorder = false;
    this._showExpensesBorder = false;
    this._showCashOnCashBorder = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    // Once the breakdown area is visible, highlight the buttons.
    if (changes.isDimmed && !changes.isDimmed.currentValue) {
      this.highlightButtons();
    }
  }

  @Input() mortgage: number;
  @Input() expenses: number;
  @Input() cashOnCash: number;
  @Input() principal: number;
  @Input() isDimmed: boolean;

  @Input() 
  set rent(rent) {
    this._rent = new Factor('rent', rent.toString());
  }

  @Output() onMortgageClicked = new EventEmitter<string>();
  @Output() onExpensesClicked = new EventEmitter<string>();
  @Output() onRentClicked = new EventEmitter<Factor>();

  highlightButtons() {
    setTimeout(() => { 
      this._showMortgageBorder = true; 
      setTimeout(() => {
        this._showRentBorder = true;
        setTimeout(() => {
          this._showExpensesBorder = true;
          setTimeout(() => {
            this._showCashOnCashBorder = true;
          }, 50);
        }, 50);
      }, 50);
    }, 1500);
  }

  openPane(paneName): void {
    switch (paneName) {
      case 'mortgage':
        this.onMortgageClicked.emit(paneName);
        break;
      case 'expenses':
        this.onExpensesClicked.emit(paneName);
        break;
      default:
        console.log('breakdown.openPane(): no pane opened.');
    }
  }

  swapMetric() {
    if (this._metric === 'default' || this._metric === 'cashOnCash') {
      this._metric = 'principal';
    } else {
      this._metric = 'cashOnCash';
    }
  }

  openInput() {
    this.onRentClicked.emit(this._rent);
  }
}
