export class MortgageFactors {
  constructor(public purchasePrice: number,
    public downPayment: number,
    public rate: number,
    public loanTerm: number,
    public propertyTax: number,
    public hoi: number) {
  }

  setPurchasePrice(purchasePrice: number) {
    this.purchasePrice = purchasePrice;
  }

  setDownPayment(downPayment: number) {
    this.downPayment = downPayment;
  }

  setRate(rate: number) {
    this.rate = rate;
  }

  setLoanTerm(loanTerm: number) {
    this.loanTerm = loanTerm;
  }

  setPropertyTax(propertyTax: number) {
    this.propertyTax = propertyTax;
  }

  setHoi(hoi: number) {
    this.hoi = hoi;
  }
}
