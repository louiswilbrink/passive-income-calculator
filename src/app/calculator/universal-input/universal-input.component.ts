import { Component, EventEmitter, Input, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { FormField } from '../form-field';
import { Factor } from '../factor';

@Component({
  selector: 'pic-universal-input',
  templateUrl: './universal-input.component.html',
  styleUrls: ['./universal-input.component.scss'],
  animations: [
    trigger('openPane', [
      state('void', style({
        position: 'absolute',
        bottom: '-100%',
      })),
      state('*', style({
        position: 'absolute',
        bottom: '0px',
      })),
      transition('void => *', animate('300ms ease-out')),
      transition('* => void', animate('300ms ease-in')),
    ])
  ]
})
export class UniversalInputComponent {
  _value: string;
  _placeholder: string;
  _field: FormField;
  _isRate: boolean;

  @Input() isOpen: string;
  @Input() decimalPlace: number;
  @Input() 
  set factor(factor) {
    this._isRate = factor.isRate;

    let value = this._isRate ? (+factor.value * 100).toString() : factor.value;

    this._field = new FormField(factor.name, value, true);
    this._placeholder = this._isRate ? Math.floor(parseFloat(this._field.value)).toString() + '%' : this._field.value;
  }

  @Output() onClose = new EventEmitter<string>();
  @Output() onAccept = new EventEmitter<Factor>();

  constructor() {
    this._value = '';
    this._placeholder = '';
    this._field = new FormField('name', '0', true);
    this._isRate = false;
  }

  resetField(event) {
    if (event.toState === 'void') { this._value = ''; }
  }

  placeDecimal(event) {
    if (this._isRate && this.decimalPlace > -1) {
      // Place the decimal point, but don't re-apply when the user hits backspace
      if (this._value.length === this.decimalPlace && event.key !== 'Backspace') {
        this._value = this._value + '.'
      }
    }
  }

  closeInput() {
    this.onClose.emit();
  }

  onAcceptClicked () {
    let newFactor: Factor;

    // If the user has not entered a new value, use the original value.
    if (this._value) {
      // if the factor is a rate, convert format.  ie: from 5% to 0.05.
      let value = this._isRate ? (+this._value / 100).toString() : this._value;

      newFactor = new Factor(this._field.name, value, this._isRate);
    } else {
      // if the factor is a rate, convert format.  ie: from 5% to 0.05.
      let value = this._isRate ? (+this._field.value / 100).toString() : this._field.value;

      newFactor = new Factor(this._field.name, value);
    }

    this.onAccept.emit(newFactor);
  }

  removePlaceholder() {
    this._placeholder = '';
  }

  resetPlaceholder() {
    if (this._value === '') {
      this._placeholder = this._isRate ? Math.floor(parseFloat(this._field.value)).toString() + '%' : this._field.value;
    }
  }
}
