import { Directive, ElementRef, HostListener, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[picActiveField]'
})
export class ActiveFieldDirective {
  constructor(private el: ElementRef) { }

  @HostListener('keyup', ['$event']) onKeyUp() {
    this.el.nativeElement.style.opacity = '1';
    this.el.nativeElement.style.color = '#FFFFFF'
  }
}
