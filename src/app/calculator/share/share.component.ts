import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Angulartics2 } from 'angulartics2';

@Component({
  selector: 'pic-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent {
  angulartics;

  @Input() isDimmed: boolean;

  @Output() onShareClicked = new EventEmitter<string>();

  constructor(angulartics2: Angulartics2) {
    this.angulartics = angulartics2;
  }

  onEmailUsClicked() {
    this.angulartics.eventTrack.next({ action: 'email-us-clicked', properties: { category: 'property' }});
  }

  shareCashflow() {
    this.onShareClicked.emit('share');
  }
}
