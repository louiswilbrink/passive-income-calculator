export class Factor {
  constructor(public name: string = '',
    public value: string = '',
    public isRate: boolean = false,
    public decimalPlace: number = -1) {
  }

  setName(name: string) {
    this.name = name;
  }

  setValue(value: string) {
    this.value = value;
  }

  setIsRate(isRate: boolean) {
    this.isRate = isRate;
  }

  setDecimalPlace(decimalPlace) {
    this.decimalPlace = decimalPlace;
  }
}
