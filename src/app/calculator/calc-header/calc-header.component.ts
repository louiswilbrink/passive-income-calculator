import { Component, Output, EventEmitter } from '@angular/core';
import { trigger, state, style } from '@angular/animations';

import { Angulartics2 } from 'angulartics2';

@Component({
  selector: 'pic-calc-header',
  templateUrl: './calc-header.component.html',
  styleUrls: ['./calc-header.component.scss']
})
export class CalcHeaderComponent {
  angulartics;

  @Output() onCleared = new EventEmitter<string>();

  constructor(angulartics2: Angulartics2) {
    this.angulartics = angulartics2;
  }

  clear() {
    this.angulartics.eventTrack.next({ action: 'calculation-cleared', properties: { category: 'property' }});

    this.onCleared.emit();
  }
}
