import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'camelToHuman'
})
export class CamelToHumanPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    // Capitalize first letter.
    // Split on all capital letters.
    // Join array of strings, separated by space.
    let newValue = (value.replace(value.charAt(0), value.charAt(0).toUpperCase())).match(/[A-Z][a-z]+/g).join(' ');
    
    return newValue;
  }
}
