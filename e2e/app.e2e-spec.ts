import { PassiveIncomeCalculatorPage } from './app.po';

describe('passive-income-calculator App', () => {
  let page: PassiveIncomeCalculatorPage;

  beforeEach(() => {
    page = new PassiveIncomeCalculatorPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
